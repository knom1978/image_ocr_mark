Find text area in the image, and then using tessaract to mark OCR result on it
===
###### tags: `EAST` `tessaract` `OpenCV` `OCR`
## Introduction
Find text area in the image, and then using tessaract to mark OCR result on it.
To achieve this goal, we use the EAST algorithm in OpenCV to find the the text are in the image.
Then we use tessaract to mark OCR result.

## How to use
a. To git clone this folder, and cd image_ocr_mark

b. Then use docker build.
```
docker build -t cecil/image-ocr-mark:1.0 .
```
c. Put your own image into the folder 'images'. Do cd images, and then you can mark them as:
```
docker run -it --rm -v "$PWD":/home/images cecil/image-ocr-mark:1.0 python3 text_detection.py --east frozen_east_text_detection.pb --min-confidence 0.5 --image images/<<your_own_image>>
```


## Reference
https://www.pyimagesearch.com/2018/09/17/opencv-ocr-and-text-recognition-with-tesseract/
https://zhuanlan.zhihu.com/p/64737915
https://zhuanlan.zhihu.com/p/64857243
https://github.com/zxdefying/OpenCV_project/tree/master/EAST_Text_Detection
