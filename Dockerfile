FROM ubuntu:18.04
LABEL maintainer="Cecil Liu <cecil_liu@umc.com>"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends libsm6 libxext6 libxrender-dev python3-setuptools python3-pip tesseract-ocr && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir /home/images

# Copy EAST model and OCR py file
COPY frozen_east_text_detection.pb /home/frozen_east_text_detection.pb
COPY text_detection.py /home/text_detection.py

# pip3 install
RUN pip3 install --upgrade pip
RUN pip3 install --no-cache-dir -U numpy imutils argparse pillow opencv-python pytesseract

ENV PYTHONIOENCODING=utf-8

WORKDIR /home/
CMD [ "bin/bash" ]
